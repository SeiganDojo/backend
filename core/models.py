from django.db import models
from django.contrib.postgres.fields import JSONField

from slugify import slugify


# ----------- Настройки сайта ------------
class SiteSettings(models.Model):
    name = models.CharField("Название сайта", max_length=100, null=False, blank=False)
    host = models.URLField("Адрес сайта", null=False, blank=False, unique=True)
    logo = models.ImageField("Логотип", upload_to='logo/')
    phone = models.CharField("Телефон", max_length=50, null=False, blank=False)
    email = models.CharField("E-mail", max_length=50, null=False, blank=False)
    copyrights = models.CharField("Копирайты", max_length=250, null=False, blank=False)
    socials = JSONField("Данные по социальным сетям")
    robots = models.TextField("Файл robots.txt", null=False, blank=False)
    redirects = models.TextField("Редиректы", null=True, blank=True)
    google = models.TextField("Google Tag Manager", null=True, blank=True)
    g_verification = models.CharField("Код подтверждения Google", blank=True, null=True, max_length=100)
    y_verification = models.CharField("Код подтверждения Yandex", blank=True, null=True, max_length=100)

    class Meta:
        verbose_name = "Настройки сайта"
        verbose_name_plural = "Настройки"

    def save(self, *args, **kwargs):
        with open('/srv/robots.txt', 'w+') as f:
            f.write(self.robots)
            f.close()
        super().save(*args, **kwargs)

    def __str__(self):
        return self.name


# ----------- Страницы -------------------
class Block(models.Model):
    name = models.CharField("Название блока", max_length=100)
    alias = models.CharField("alias блока для фронтенда", max_length=100)
    data = JSONField()

    class Meta:
        verbose_name = "Блок"
        verbose_name_plural = "Блоки"

    def __str__(self):
        return self.name

class MetaTemplate(models.Model):
    name = models.CharField("Название шаблона", max_length=100, null=False, blank=False)
    title = models.CharField("META: title", max_length=200, null=True, blank=True)
    keywords = models.CharField("META: keywords", max_length=200, null=True, blank=True)
    description = models.CharField("META: description", max_length=200, null=True, blank=True)

    class Meta:
        verbose_name = "Шаблон МЕТА"
        verbose_name_plural = "Шаблоны МЕТА"

    def __str__(self):
        return self.name


class Page(models.Model):
    name = models.CharField("Название страницы", max_length=100)
    alias = models.CharField("alias страницы для фронтенда", max_length=100, null=True, blank=True, unique=True)
    blocks = models.ManyToManyField(Block, through='PageBlock')
    top_menu = models.BooleanField("В верхнем меню", default=True)
    top_menu_order = models.PositiveSmallIntegerField("Порядок в верхнем меню", null=True, blank=True, unique=True)

    meta_title = models.CharField("META: title", max_length=200, null=True, blank=True)
    meta_keywords = models.CharField("META: keywords", max_length=200, null=True, blank=True)
    meta_description = models.CharField("META: description", max_length=200, null=True, blank=True)

    meta_template = models.ForeignKey("MetaTemplate", null=True, blank=True, on_delete=models.SET_NULL)


    class Meta:
        verbose_name = "Страница"
        verbose_name_plural = "Страницы"

    def __str__(self):
        return self.name


class PageBlock(models.Model):
    page = models.ForeignKey(Page, verbose_name="Страница",  on_delete=models.CASCADE)
    block = models.ForeignKey(Block, verbose_name="Блок", on_delete=models.CASCADE)
    order = models.PositiveSmallIntegerField("Порядок вывода")

    class Meta:
        unique_together = ['order', 'page']


# ------------- Товары ---------------
class CalcType(models.Model):
    name = models.CharField("Название расчёта", max_length=120)
    parts = models.PositiveSmallIntegerField("Количество частей")

    class Meta:
        verbose_name = "Тип расчёта"
        verbose_name_plural = "Типы расчёта"

    def __str__(self):
        return self.name


class Category(models.Model):
    name = models.CharField("Название категории", max_length=100)
    slug = models.CharField("SLUG категории", max_length=100, null=True, blank=True)
    description = models.TextField("Описание категории", null=False, blank=False)
    image = models.ImageField("Изображение категории", upload_to='category/')
    calc_types = models.ManyToManyField(CalcType, verbose_name="Расчёты", blank=True, null=True)

    class Meta:
        verbose_name = "Категория"
        verbose_name_plural = "Категории"

    def __str__(self):
        return self.name

    def save(self, *args, **kwargs):
        self.slug = slugify(self.name)
        super().save(*args, **kwargs)


WIDTH_SMALL = 1
WIDTH_WIDE = 2
WIDTH_TYPES = (
    (WIDTH_SMALL, "Узкий"),
    (WIDTH_WIDE, "Широкий")
)


class Product(models.Model):
    name = models.CharField("Название товара", max_length=100, null=False, blank=False)
    slug = models.CharField("SLUG продукта", max_length=100, null=True, blank=True)
    description = models.TextField("Описание товара", null=False, blank=False)
    category = models.ForeignKey(
        Category,
        verbose_name="Категория товара",
        on_delete=models.CASCADE,
        blank=False,
        null=False
    )
    width = models.PositiveSmallIntegerField("Ширина блока", choices=WIDTH_TYPES, default=WIDTH_SMALL)
    calculator = models.BooleanField("Калькулятор", default=False)
    image = models.ImageField("Изображение товара", upload_to='product/')
    base_price = models.PositiveIntegerField("Начальная цена")
    meta_title = models.CharField("META: title", max_length=200, null=True, blank=True)
    meta_keywords = models.CharField("META: keywords", max_length=200, null=True, blank=True)
    meta_description = models.CharField("META: description", max_length=200, null=True, blank=True)

    meta_template = models.ForeignKey("MetaTemplate", null=True, blank=True, on_delete=models.SET_NULL)

    class Meta:
        verbose_name = "Товар"
        verbose_name_plural = "Товары"

    def __str__(self):
        return self.name

    def save(self, *args, **kwargs):
        self.slug = slugify(self.name)
        super().save(*args, **kwargs)


class Question(models.Model):
    phone = models.CharField("Контакт клиента", max_length=100)
    name = models.CharField("Контакт клиента", max_length=100)
    date = models.DateTimeField("Дата заявкки", auto_now_add=True, null=True)

    class Meta:
        verbose_name = "Контакт клиента"
        verbose_name_plural = "Контакты клиентов"

    def __str__(self):
        return self.date.strftime("%d-%m-%Y %H:%M:%S, ") + "Телефон: " + self.phone + ", Имя: " + self.name


class Order(models.Model):
    date = models.DateTimeField("Дата заявкки", auto_now_add=True, null=True)
    data = JSONField("Данные заказа", null=False, blank=True)

    class Meta:
        verbose_name = "Заказ"
        verbose_name_plural = "Заказы"

    def __str__(self):
        return self.date.strftime("%d-%m-%Y %H:%M:%S, ") + "Телефон: " + self.phone + ", Имя: " + self.name


class Slider(models.Model):
    title = models.CharField("Заголовок слайда", max_length=100)
    text = models.CharField("Текст слайда", max_length=200)
    link = models.CharField("Ссылка на кнопке", max_length=100)
    background = models.ImageField(
        "Изображение на слайдере",
        upload_to='slides'
    )

    class Meta:
        verbose_name = "Слайд"
        verbose_name_plural = "Слайды"

    def __str__(self):
        return self.title
