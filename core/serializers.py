from .models import *
from rest_framework import serializers


def get_page_meta(obj, ver=None):
    if obj.meta_title:
        title = obj.meta_title
    else:
        if obj.meta_template:
            title = obj.meta_template.title
        else:
            title = obj.name

    response = {
        "title": title,
        "description": obj.meta_description,
        "keywords": obj.meta_keywords,
    }

    if ver:
        response["y_verification"] = ver.get("yandex")
        response["g_verification"] = ver.get("google")

    return response


class MenuSerializer(serializers.ModelSerializer):
    class Meta:
        model = Page
        fields = ("name", "alias")


class CategorySerializer(serializers.ModelSerializer):
    class Meta:
        model = Category
        fields = '__all__'


class CalcTypeSerializer(serializers.ModelSerializer):
    class Meta:
        model = CalcType
        fields = '__all__'


class ProductSerializer(serializers.ModelSerializer):
    meta = serializers.SerializerMethodField('get_meta', read_only=True)
    calc_types = serializers.SerializerMethodField('get_calc_types', read_only=True)

    class Meta:
        model = Product
        fields = ["id", "name", "description", "slug", "width", "calculator", "image", "base_price", "meta", "category", "calc_types"]


    def get_meta(self, obj):
        if obj.meta_title:
            title = obj.meta_title
        else:
            if obj.meta_template:
                title = obj.meta_template.title
            else:
                title = f"{obj.name} | {obj.category.name} - СВ Окна"

        if obj.meta_description:
            description = obj.meta_description
        else:
            description = obj.meta_template.description if obj.meta_template else ""

        if obj.meta_keywords:
            keywords = obj.meta_keywords
        else:
            keywords = obj.meta_template.keywords if obj.meta_template else ""

        response = {
            "title": title,
            "description": description,
            "keywords" : keywords
        }
        return response

    def get_calc_types(self, obj):
        category = obj.category
        calc_types = category.calc_types.all()
        return CalcTypeSerializer(calc_types, many=True).data


class BlockSerializer(serializers.ModelSerializer):
    class Meta:
        model = Block
        exclude = ('id', "name")


class PageBlockSerializer(serializers.ModelSerializer):
    class Meta:
        model = PageBlock
        exclude = ('page', 'id')


class SiteSettingsSerializer(serializers.ModelSerializer):
    menu = serializers.SerializerMethodField(read_only=True)
    main = serializers.SerializerMethodField(read_only=True)
    metrics = serializers.SerializerMethodField(read_only=True)
    redirect = serializers.SerializerMethodField(read_only=True)
    catalog = serializers.SerializerMethodField(read_only=True)
    services = serializers.SerializerMethodField(read_only=True)

    def get_catalog(self, obj):
        return [{"name": "test", "alias": "test"}]

    def get_services(self, obj):
        return [{"name": "test", "alias": "test"}]

    def get_menu(self, obj):
        pages = Page.objects.filter(top_menu=True).order_by('top_menu_order')
        menu = MenuSerializer(pages, many=True).data
        return menu

    def get_main(self, obj):
        main_page = Page.objects.get(alias="main")
        block_qs = PageBlock.objects.filter(page=main_page).order_by('order')
        structure = []
        for p_block in block_qs:
            block = p_block.block
            data = BlockSerializer(block).data
            structure.append(data)

        settings_obj = SiteSettings.objects.first()
        verification = {
            "yandex": settings_obj.y_verification,
            "google": settings_obj.g_verification
        }

        meta = get_page_meta(main_page, verification)
        response = {
            "structure": structure,
            "meta": meta
        }
        return response

    def get_metrics(self, obj):
        response = {"google": obj.google}
        return response

    def get_redirect(self, obj):
        if obj.redirects:
            redirects = obj.redirects.splitlines()
            response = [{"from": line.split(" ")[0], "to": line.split(" ")[1]} for line in redirects]
        else:
            response = []
        return response

    class Meta:
        model = SiteSettings
        exclude = ('id', 'name', "redirects", "google", "robots")


class QuestionSerializer(serializers.ModelSerializer):
    class Meta:
        model = Question
        fields = '__all__'


class OrderSerializer(serializers.ModelSerializer):
    class Meta:
        model = Order
        fields = '__all__'


class SliderSerializer(serializers.ModelSerializer):
    class Meta:
        model = Slider
        fields = '__all__'


class PageSerializer(serializers.ModelSerializer):
    meta = serializers.SerializerMethodField('get_meta', read_only=True)
    structure = serializers.SerializerMethodField('get_structure', read_only=True)


    class Meta:
        model = Page
        fields = ["meta", "structure", "id", "name", "alias", "top_menu", "top_menu_order"]

    def get_structure(self, obj):
        page_blocks = PageBlock.objects.filter(page=obj.id).order_by('order')
        structure = []
        for p_block in page_blocks:
            block = p_block.block
            data = BlockSerializer(block).data
            structure.append(data)
        return structure


    def get_meta(self, obj):
        response = get_page_meta(obj)
        return response


