from django.urls import include, path
from rest_framework.routers import DefaultRouter
from .views import *

router = DefaultRouter()

router.register(r'category', CategoryViewSet, basename='category')
router.register(r'product', ProductViewSet, basename='product')
router.register(r'settings', SiteSettingsViewSet, basename='settings')
router.register(r'question', QuestionViewSet, basename='question')
router.register(r'slider', SliderViewSet, basename='slider')
router.register(r'pages', PageViewSet, basename='pages')


urlpatterns = [
    path('', include(router.urls)),
    path('robots', RobotsViews.as_view(), name='robots')
]