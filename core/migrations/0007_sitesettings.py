# Generated by Django 3.0.6 on 2020-05-13 19:47

import django.contrib.postgres.fields.jsonb
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0006_auto_20200512_2317'),
    ]

    operations = [
        migrations.CreateModel(
            name='SiteSettings',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=100, verbose_name='Название сайта')),
                ('host', models.URLField(unique=True, verbose_name='Адрес сайта')),
                ('logo', models.ImageField(upload_to='logo/', verbose_name='Логотип')),
                ('phone', models.CharField(max_length=50, verbose_name='Телефон')),
                ('email', models.CharField(max_length=50, verbose_name='E-mail')),
                ('copyrights', models.CharField(max_length=50, verbose_name='Копирайты')),
                ('socials', django.contrib.postgres.fields.jsonb.JSONField(verbose_name='Данные по социальным сетям')),
            ],
            options={
                'verbose_name': 'Настройки сайта',
                'verbose_name_plural': 'Настройки',
            },
        ),
    ]
