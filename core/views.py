from django_filters import rest_framework as dj_filters

from django.shortcuts import render

from rest_framework.views import APIView
from rest_framework import viewsets
from rest_framework.authentication import TokenAuthentication
from rest_framework.response import Response
from rest_framework.decorators import action

from .models import *
from .serializers import *


class DefaultViewSet(viewsets.ReadOnlyModelViewSet):
    model = None

    def get_queryset(self):
        self.queryset = self.model.objects.all()
        return super().get_queryset()


class SiteSettingsViewSet(DefaultViewSet):
    model = SiteSettings
    serializer_class = SiteSettingsSerializer


class CategoryViewSet(DefaultViewSet):
    model = Category
    serializer_class = CategorySerializer


class ProductFilter(dj_filters.FilterSet):
    category = dj_filters.CharFilter(field_name="category__slug")

    class Meta:
        model = Product
        fields = {}


class ProductViewSet(DefaultViewSet):
    model = Product
    serializer_class = ProductSerializer
    filter_backends = [dj_filters.DjangoFilterBackend]
    filterset_class = ProductFilter
    lookup_field = "slug"


class QuestionViewSet(viewsets.ViewSet):
    serializer_class = QuestionSerializer

    @action(detail=False, methods=['post'])
    def ask(self, request):
        serializer = self.serializer_class(data=request.data)

        if serializer.is_valid():
            serializer.save()
            response = {
                "success": True,
            }
        else:
            response = {
                "success": False,
                "data": serializer.errors
            }
        return Response(response)


class OrderViewSet(viewsets.ModelViewSet):
    queryset = Order.objects.all()
    serializer_class = OrderSerializer


class SliderViewSet(viewsets.ViewSet):
    serializer_class = SliderSerializer

    @action(detail=False, methods=['get'])
    def get_slides(self, request):
        slides = Slider.objects.all()
        serializer = self.serializer_class(slides, many=True)
        response = serializer.data
        return Response(response)


class PageViewSet(viewsets.ReadOnlyModelViewSet):
    queryset = Page
    serializer_class = PageSerializer
    lookup_field = "alias"


class RobotsViews(APIView):
    def get(self, request):
        settings = SiteSettings.objects.get(id=1)
        robots_data = settings.robots
        return Response(robots_data)
