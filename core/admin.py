from django.contrib import admin
from .models import *
from django.contrib.postgres import fields
from django_json_widget.widgets import JSONEditorWidget


@admin.register(Block)
class BlockAdmin(admin.ModelAdmin):
    formfield_overrides = {
        fields.JSONField: {'widget': JSONEditorWidget},
    }


class PageBlockInline(admin.TabularInline):
    model = PageBlock
    extra = 1


@admin.register(Page)
class PageAdmin(admin.ModelAdmin):
    inlines = (PageBlockInline,)


@admin.register(CalcType)
class CalcTypeAdmin(admin.ModelAdmin):
    pass


@admin.register(Category)
class CategoryAdmin(admin.ModelAdmin):
    pass


@admin.register(Product)
class ProductAdmin(admin.ModelAdmin):
    pass


@admin.register(SiteSettings)
class SiteSettingsAdmin(admin.ModelAdmin):
    pass


@admin.register(Question)
class QuestionAdmin(admin.ModelAdmin):
    pass


@admin.register(Order)
class OrderAdmin(admin.ModelAdmin):
    pass


@admin.register(Slider)
class SliderAdmin(admin.ModelAdmin):
    pass

@admin.register(MetaTemplate)
class MetaTemplateAdmin(admin.ModelAdmin):
    pass